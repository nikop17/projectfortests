package com.nireapp.projectfortests;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.nireapp.projectfortests.recycler.RecyclerFragment;

public class HostActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host);

        final FrameLayout cvContaineer = findViewById(R.id.container);
        final Button rlvButton = findViewById(R.id.btn_recycler);
        rlvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(HostActivity.class.getName(), ": button clicked");
                cvContaineer.setVisibility(View.VISIBLE);
                Fragment recyclerFrmnt = RecyclerFragment.newInstance(null, null, RecyclerFragment.class.getName());
                navigate(recyclerFrmnt);
                rlvButton.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_recycler:
                Fragment recyclerFrmnt = RecyclerFragment.newInstance(null, null, RecyclerFragment.class.getName());
                navigate(recyclerFrmnt);
                break;
            default:
                break;
        }
    }

    private void navigate(Fragment fragment) {
        Log.i(HostActivity.class.getName(), ": navigate called");
        if (fragment != null) {
            Log.i(HostActivity.class.getName(), ": fragment not null");
            FragmentManager frm = getSupportFragmentManager();
            if (frm.getBackStackEntryCount() == 0) {
                Log.i(HostActivity.class.getName(), ": no fragments at back stack");
                frm.beginTransaction().add(R.id.container, fragment, fragment.getTag()).commit();
            } else {
                Log.i(HostActivity.class.getName(), ": replacing fragment");
                frm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).replace(R.id.container, fragment, fragment.getTag()).addToBackStack(" ").commit();
            }
        }
    }
}
