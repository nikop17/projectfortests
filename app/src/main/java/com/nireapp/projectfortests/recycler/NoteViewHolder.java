package com.nireapp.projectfortests.recycler;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nireapp.projectfortests.R;

public class NoteViewHolder extends RecyclerView.ViewHolder {

    CardView cvNoteField;
    TextView tvDate;
    TextView tvBody;
    CheckBox chBoxState;

    public NoteViewHolder(View view) {
        super(view);
        cvNoteField = view.findViewById(R.id.cv_list_item);
        tvDate = view.findViewById(R.id.tv_note_date);
        tvBody = view.findViewById(R.id.tv_note_body);
        chBoxState = view.findViewById(R.id.chbx_expand_collapse);
    }

}
