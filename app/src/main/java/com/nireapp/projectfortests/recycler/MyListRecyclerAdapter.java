package com.nireapp.projectfortests.recycler;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.nireapp.projectfortests.R;
import com.nireapp.projectfortests.data.Note;
import com.nireapp.projectfortests.recycler.helpers.ItemTouchHelperAdapter;

import java.util.List;

/*
    Implement on click actions here
*/
public class MyListRecyclerAdapter extends RecyclerView.Adapter<NoteViewHolder> implements View.OnClickListener, ItemTouchHelperAdapter {

    private List<Note> dataList;

    public MyListRecyclerAdapter(List<Note> objectList) {
        dataList = objectList;
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_list_item, parent, false);
        cv.setOnClickListener(this);
        NoteViewHolder nViewHolder = new NoteViewHolder(cv);

        return nViewHolder;
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        Log.i(MyListRecyclerAdapter.class.getName(), "Binding view holder");
        final int i = position;
        if (dataList != null) {
            if (dataList.get(position).getNoteFalg() == Note.SHOWN) {
                holder.chBoxState.setChecked(true);
            } else {
                holder.chBoxState.setChecked(false);
            }
            holder.chBoxState.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(MyListRecyclerAdapter.class.getName(), "position clicked is " + i);
                    CheckBox rv = (CheckBox) v;
                    if (rv.isChecked()) {
                        Log.d(MyListRecyclerAdapter.class.getName(), "on click work checked " + i);
                        dataList.get(i).setNoteFalg(Note.SHOWN);
                    } else {
                        Log.d(MyListRecyclerAdapter.class.getName(), "on click work unchecked " + i);
                        dataList.get(i).setNoteFalg(Note.HIDDEN);
                    }
                    notifyItemChanged(i);
                }
            });
            holder.tvDate.setText(dataList.get(position).getNoteDate());
            holder.tvBody.setText(dataList.get(position).getNoteData(dataList.get(position).getNoteFalg()));
        } else {
            Log.d(MyListRecyclerAdapter.class.getName(), "data list is null");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.layout.card_list_item:
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (dataList != null) {
            return dataList.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDismiss(int position) {
        Log.d(MyListRecyclerAdapter.class.getName(), "called method on item dismiss");
        dataList.remove(position);
        notifyItemRemoved(position);
//        notifyDataSetChanged();
    }
}
