package com.nireapp.projectfortests.recycler;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nireapp.projectfortests.R;
import com.nireapp.projectfortests.data.Note;
import com.nireapp.projectfortests.recycler.helpers.ItemTouchHelperAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RecyclerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecyclerFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private List<Note> data;


    public RecyclerFragment() {
        // Required empty public constructor
    }


    public static RecyclerFragment newInstance(String param1, String param2, String tag) {
        RecyclerFragment fragment = new RecyclerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = makeTestData();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler, null, false);

        RecyclerView listView = view.findViewById(R.id.rv_list);
        listView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        listView.setLayoutManager(layoutManager);

        final MyListRecyclerAdapter adapter = new MyListRecyclerAdapter(data);
        listView.setAdapter(adapter);
        final ItemTouchHelperAdapter adapt = adapter;
//        ItemTouchHelper.Callback ithCallback = new SimpleItemTouchHelperCallback(adapt);
        ItemTouchHelper touchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                adapt.onItemDismiss(viewHolder.getAdapterPosition());
                //Toast.makeText(getContext(), "Swiped " + direction , Toast.LENGTH_SHORT).show();
            }
        });
        touchHelper.attachToRecyclerView(listView);

        return view;
    }

    private List<Note> makeTestData() {
        List<Note> dataSet = new ArrayList();
        Note n = new Note();
        n.setNoteId(1);
        n.setNoteData("Я уверен в том, что мы прокляты и обречены всегда думать, что можно было сделать немного лучше, даже когда добиваемся всего, чего хотели.");
        Date date = new Date();
        n.setNoteDate(date.toString());
        n.setNoteFalg(Note.SHOWN);
        dataSet.add(n);
        n = new Note();
        n.setNoteId(2);
        n.setNoteData("Отчаяние — это толчок к изучению или созданию чего-то нового. Если у вас не бывают периодов отчаяния — вы не развиваетесь.");
        date = new Date();
        n.setNoteDate(date.toString());
        n.setNoteFalg(Note.HIDDEN);
        dataSet.add(n);
        n = new Note();
        n.setNoteId(3);
        n.setNoteData("Отчаяние — это толчок к изучению или созданию чего-то нового. Если у вас не бывают периодов отчаяния — вы не развиваетесь.");
        date = new Date();
        n.setNoteDate(date.toString());
        n.setNoteFalg(Note.HIDDEN);
        dataSet.add(n);
        n = new Note();
        n.setNoteId(4);
        n.setNoteData("Ничего не принимайте близко к сердцу. Немногое на свете долго бывает важным.");
        date = new Date();
        n.setNoteDate(date.toString());
        n.setNoteFalg(Note.SHOWN);
        dataSet.add(n);
        n = new Note();
        n.setNoteId(5);
        n.setNoteData("Самая большая глупость — это делать тоже самое и надеяться на другой результат.");
        date = new Date();
        n.setNoteDate(date.toString());
        n.setNoteFalg(Note.SHOWN);
        dataSet.add(n);
        Log.i(RecyclerFragment.class.getName(), "Test data generated");
        return dataSet;
    }
}
